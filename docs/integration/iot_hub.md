# Integrating IoTC with Azure IoT Hub

This documentation provides step-by-step instructions on how to integrate EnOcean IoT Connector with Azure IoT Hub. 

EnOcean IoT Connector can be easily setup to serve as the data source for [Azure IoT Hub](https://azure.microsoft.com/en-us/products/iot-hub). Azure IoT Hub enables highly secure and reliable communication between your Internet of Things (IoT) application and the devices it manages and it provides a cloud-hosted solution back end to connect virtually any device.

## Step-by-step deployment

### Step 1: Create a Device in Azure IoT Hub

1. Log in to Azure Portal:

    - Open your web browser and navigate to the Azure Portal.
    - Log in with your Azure account.

2. Navigate to Azure IoT Hub:

    - In the Azure Portal, search for "IoT Hub" and select it from the results.

3. Create a New Device:

    - In the IoT Hub pane, click on "IoT devices" in the left navigation menu.
    - Click the "New" button to add a new device.
    - Fill in the necessary details and click "Save."

4. Get the Connection String:

    - Select the created device.
    - Under the "Device details" tab, copy the "Primary connection string"

### Step 2: Update Docker Compose File
- Add variables below to *integration* containers environment variables on docker-compose.yml. Please use provided docker-compose.yml as a guideline.

```yaml
- IOT_AZURE_ENABLE=1
- IOT_AZURE_CONNSTRING=  # specify the connection string
```

### Step 3: Deploy and Test
1. Deploy IoTC:

    - Run docker-compose up -d to deploy IoTC with the updated Docker Compose file.

2. Verify Integration:

    - Monitor logs to ensure successful integration with Azure IoT Hub.