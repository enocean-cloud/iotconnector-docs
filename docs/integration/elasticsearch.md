# Elasticsearch Setup (Linux)
* Step1: download the PGP key
 ~~~~
  wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
 ~~~~
### For v8.*
* Step2: Install apt-transport and "8.* apt repo"
 ~~~~
  sudo apt-get install apt-transport-https
  echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
 ~~~~
* Step3: Install elasticsearch
  Save the password that will be generated for the user "elastic"
 ~~~~
  sudo apt-get update && sudo apt-get install elasticsearch
 ~~~~
* Step4: Enable on boot and start the service.
 ~~~~
  sudo systemctl daemon reload
  sudo systemctl start elasticsearch.service
 ~~~~
* Step5: Confirm by sending a request to "https://localhost:9200"
  Input the user "elastic" and generated password, you shoul recieve a response like so.
 ~~~~
  {
    "name" : "Cp8oag6",
    "cluster_name" : "elasticsearch",
    "cluster_uuid" : "AT69_T_DTp-1qgIJlatQqA",
    "version" : {
      "number" : "8.4.1",
      "build_type" : "tar",
      "build_hash" : "f27399d",
      "build_flavor" : "default",
      "build_date" : "2016-03-30T09:51:41.449Z",
      "build_snapshot" : false,
      "lucene_version" : "9.3.0",
      "minimum_wire_compatibility_version" : "1.2.3",
      "minimum_index_compatibility_version" : "1.2.3"
    },
    "tagline" : "You Know, for Search"
  }
 ~~~~
* Step6: Create a user and password for kibana(v8.*) system.
 ~~~~
 /usr/share/elasticsearch/bin/elasticsearch-reset-password -u kibana_system
 password="password"
 ~~~~
* Step7: To access elasticsearch from another machine you have to make the following changes to the configuration file in elasticsearch.
go to `/etc/elasticsearch/elasticsearch.yml` and append the following configurations. Elasticsearch v8.* comes with security enabled by default, so all incoming connections either uses basic security authentication or certificates for connections.

``` 
network.host: 0.0.0.0
http.port: 9200
discovery.seed_hosts: ["0.0.0.0", "[::1]"]
```

Security configuration should be like the sample below.

``` 
# Enable security features
xpack.security.enabled: true

xpack.security.enrollment.enabled: true

# Enable encryption for HTTP API client connections, such as Kibana, Logstash, and Agents
xpack.security.http.ssl:
 enabled: true
 keystore.path: certs/http.p12

# Enable encryption and mutual authentication between cluster nodes
xpack.security.transport.ssl:
 enabled: true
 verification_mode: certificate
 keystore.path: certs/transport.p12
 truststore.path: certs/transport.p12
# Create a new cluster with the current node only
# Additional nodes can still join the cluster later
cluster.initial_master_nodes: ["<your-node-name>"]

# Allow HTTP API connections from anywhere
# Connections are encrypted and require user authentication
http.host: 0.0.0.0
```
Go to `/etc/elasticsearch/certs/` and copy "http_ca.crt" you will need it for encryted connection from kibana(v8.*).

### For v7.* (linux)

Repeat step1 above.
* Step2: Install apt-transport and "7.* apt repo"
 ~~~~
  sudo apt-get install apt-transport-https
  echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
 ~~~~
* Step3: Install elasticsearch
 ~~~~
  sudo apt-get update && sudo apt-get install elasticsearch
 ~~~~
* Step4: Enable on boot and start the service.
 ~~~~
  sudo systemctl daemon reload
  sudo systemctl start elasticsearch.service
 ~~~~
* Step5: send a request to http://localhost:9200 you should get a response like so.

 ~~~~
 {
  "name" : "Cp8oag6",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "AT69_T_DTp-1qgIJlatQqA",
  "version" : {
    "number" : "7.17.6",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "f27399d",
    "build_date" : "2016-03-30T09:51:41.449Z",
    "build_snapshot" : false,
    "lucene_version" : "8.11.1",
    "minimum_wire_compatibility_version" : "1.2.3",
    "minimum_index_compatibility_version" : "1.2.3"
  },
  "tagline" : "You Know, for Search"
 }
 ~~~~

* Step6: To access the cluster from a different machine modify the configuration file at `/etc/elasticsearch/elasticsearch.yml` with 
the following values; and restart the service.

``` 
network.host: 0.0.0.0
http.port: 9200
discovery.seed_hosts: ["0.0.0.0", "[::1]"]
```

### NOTE
Elasticsearch v7.* does not have security enabled by default, to configure basic security and certificates follow the steps below

### Security Setup in Elasticsearch v7*
#### minimal security (users and passwords)
* Step1: Go to `/path/to/elasticseach.yml` and add the following to the file;
~~~~
xpack.security.enabled: true
~~~~
* Step2: Create passwords for built-in/new users
~~~~
./bin/elasticsearch-setup-passwords interactive -u (username)
~~~~
restart the service

#### Secured HTTPS traffic for Elasticsearch v7*
Using the elasticsearch certutil tool helps to create the neccesary certificates used by elasticsearch in this mode.
Go to the directory where elasticsearch binary executables are, they are usually at `/usr/share/elasticsearch/bin`
* Step1: Certificate signing request (CSR).
~~~~
/usr/share/elasticsearch/bin/elasticsearch-certutil http
~~~~

when prompted for CSR type "n" for no. You could say yes if you have an already existing CA authority that you do not control to sign your certificates.
when promted to use an existing CA type "n" for no. You could say yes, if you already previously generated one.
A new CA will be generated, provide the neccesary details, Hostnames(if available) and ip addresses depending on your setup.
A new zip file `/usr/share/elasticsearch/elasticsearch-ssl-http.zip` is created.

* Step2: unzip the file
~~~~
unzip elasticsearch-ssl-http.zip
~~~~ 
After unziping, two folders are created, one for elasticsearch and one for kibana, they both contain three(3) files, they are the certificate `http.p12`, Readme.txt and a sample-elasticsearch.yml and also `elasticsearch-ca.pem`, Readme.txt and a sample-kibana.yml these files contain next steps for the configuration.

* Step3: copy the certificate to your elasticsearch configuration directory. and add the following to your elasticsearch.yml file
~~~~
xpack.security.http.ssl.enabled: true
xpack.security.http.ssl.keystore.path: "http.p12"
~~~~
save and restart elasticsearch
For Kibana(v7.*) go to kibana.yaml and update "elasticsearch.hosts: [ 'http://localhost:9200' ]" to "elasticsearch.hosts: [ 'https://<your_elastic_search_ip>:9200' ]"
Copy "elasticsearch-ca.pem" file directly into the kibana config directory without renaming it.
uncomment and modify `elasticsearch.ssl.certificateAuthorities: $KBN_PATH_CONF/elasticsearch-ca.pem`

# Elasticsearch setup for windows
## v8.* setup (windows)
* Step1: download the .zip archive
~~~~
https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.4.2-windows-x86_64.zip
~~~~
* Step2: extract it and go to `configs/elasticsearch.yml`
 uncomment and modify these values.

``` 
network.host: 0.0.0.0
http.port: 9200
discovery.seed_hosts: ["0.0.0.0", "[::1]"]
```

* step3: start elasticsearch
~~~~
.\bin\elasticsearch.bat
~~~~
password credentails with be output to terminal
* step4: certificate is located at `configs/certs`
copy certificate to your kibana config/installation directory

For Kibana(v8.*) go to kibana.yaml and update "elasticsearch.hosts: [ 'http://localhost:9200' ]" to "elasticsearch.hosts: [ 'https://<your_elastic_search_ip>:9200' ]"
Copy the cert from `configs/certs` above into the kibana config directory without renaming it.
uncomment and modify `elasticsearch.ssl.certificateAuthorities: $KBN_PATH_CONF/'cert'`.

## v7.* setup (windows)
* Step1: download the .zip archive
~~~~
https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.17.6-windows-x86_64.zip
~~~~
Repeat step2 and step3 above.