# Kibana Setup
## v8.* (linux)
* Step1: download the pgp key;
~~~~
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
~~~~
* Step2: Install apt-transport and elastic-repo for v8.*
~~~~
sudo apt-get install apt-transport-https
echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
~~~~
* Step3: install kibana
~~~~
sudo apt-get update && sudo apt-get install kibana
~~~~
you can generate an enrolment token for kibana with the following command;
~~~~
bin/elasticsearch-create-enrollment-token -s kibana   # You can either use this or proceed with the certificate provided by elasticsearch
~~~~
* Step4: configure kibana
add the following to your config/kibana.yml
~~~~
server.port: 5601
server.host: "localhost" or 0.0.0.0
server.publicBaseUrl: "http://localhost:5601"
elasticsearch.hosts: ["https://<elasticsearch_ip:9200"]
elasticsearch.username: "kibana_system"    # this username and password is valid when you have created a user "kibana_system" in your elasticsearch node.
elasticsearch.password: "password"
elasticsearch.ssl.certificateAuthorities: [ "path/to/cert/gotten/from/elasticsearch/http_ca.crt" ]
elasticsearch.ssl.verificationMode: certificate
~~~~ 
* Step5: Enable on boot and start the service
~~~~
sudo systemctl daemon reload
sudo systemctl start kibana.service
~~~~
* Step6: Access kibana
~~~~
You can access kibana on http://<IP:5601
~~~~

## v7.* setup (linux)
Same steps for v8.* except step2 and 4
* Step2
~~~~
sudo apt-get install apt-transport-https
echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
~~~~
* Step4: configure kibana
add the following to your config/kibana.yml
~~~~
server.port: 5601
server.host: "localhost" or 0.0.0.0
server.publicBaseUrl: "http://localhost:5601"
elasticsearch.hosts: ["https://<elasticsearch_ip:9200"] if security is configured elasticsearch.hosts: ["http://elasticsearch_ip:9200"]
elasticsearch.username: "kibana_system"    # this username and password is valid when you have created a user "kibana_system" in your elasticsearch node.
elasticsearch.password: "password"
elasticsearch.ssl.certificateAuthorities: [ "path/to/cert/gotten/from/elasticsearch/http_ca.crt" ] # comment this out if security is not configured
elasticsearch.ssl.verificationMode: certificate # comment this out if security is not configured
~~~~ 

## v8.* (windows)
* Step1: Download the .zip file
~~~~
https://artifacts.elastic.co/downloads/kibana/kibana-8.4.2-windows-x86_64.zip
~~~~
* Step2: extract the files. and configure Kibana.
go to configs/kibana.yaml, from the previous configurations in linux, input the neccesary values
* Step3: start the service
~~~~
.\bin\kibana.bat
~~~~