# Configuration using Aruba Instant

## Required Hardware and Software

+ Aruba AP: Aruba AP with USB port. 

    Check the energy requirements of our Aruba AP to properly operate the USB port. 

![](../img/aruba-ap-power-req.png)

+ Aruba OS: version **8.10.0.13** or higher (most likely requires update to latest).

+ [EnOcean USB Stick](https://www.enocean.com/en/products/distributor/): USB 300, USB 300U, USB 500 or USB 500U

## Step 1: Connect to Instant

Log into the router’s web-based management page for Aruba instant.
<!-- pic below a bit smaller -->

![](../img/aruba-setup/instant-login.png){width=80% height=80%}

## Step 2: Installing Trusted CA Certificates

!!! note
    When using Public Validated Certificates with Aruba Central, the complete certificate path (root + intermediate CAs) must be installed as **one file**. 

+  Upload your .pem certificate file From **Maintenance** -> **Certificates** -> **Upload new certificate**.

    select **Trusted CA** as **certificate type** and **x509(.pem .cer or .crt)** as **Certificate format**.

![](../img/aruba-setup/CA-upload-page.png){width=70% height=70%}

+ Browse to certificate. Certificate must be provided as **one file** including complete certificate path (root + intermediate CA).
To create the certificate file chain for public validated certificates follow the guide : [Using chained certificate](using-chained-certificate.md).


+ Click **Upload Certificate** to save your settings.

### Verify Certificate upload.

Verify the certificate is shown on the certificate list:

- Using the UI:

![](../img/aruba-setup/ui-ca.png)

- Using the CLI: 

    ```powershell
        (IAP)# show cert all

        Cert type:TrustedCA
        Cert name:GTS-CA-1D4-chain
        Version       :2
        Serial Number :02008EB2023336658B64CDDB9B
        Issuer        :/C=US/O=Google Trust Services LLC/CN=GTS Root R1
        Subject       :/C=US/O=Google Trust Services LLC/CN=GTS CA 1D4
        Issued On     :Aug 13 00:00:42 2020 GMT
        Expires On    :Sep 30 00:00:42 2027 GMT
        RSA Key size  :2048 bits
        Signed Using  :RSA-SHA256

        Version       :2
        Serial Number :77BD0D6CDB36F91AEA210FC4F058D30D
        Issuer        :/C=BE/O=GlobalSign nv-sa/OU=Root CA/CN=GlobalSign Root CA
        Subject       :/C=US/O=Google Trust Services LLC/CN=GTS Root R1
        Issued On     :Jun 19 00:00:42 2020 GMT
        Expires On    :Jan 28 00:00:42 2028 GMT
        RSA Key size  :4096 bits
        Signed Using  :RSA-SHA256

        Version       :2
        Serial Number :040000000001154B5AC394
        Issuer        :/C=BE/O=GlobalSign nv-sa/OU=Root CA/CN=GlobalSign Root CA
        Subject       :/C=BE/O=GlobalSign nv-sa/OU=Root CA/CN=GlobalSign Root CA
        Issued On     :Sep  1 12:00:00 1998 GMT
        Expires On    :Jan 28 12:00:00 2028 GMT
        RSA Key size  :2048 bits
        Signed Using  :RSA-SHA1

    ```

## Step 3: IoT transport configuration

+ In Aruba Instant select **Configuration** -> **Services** -> **IoT** -> **IoT transports** then add a new transport using the **+** icon:

![](../img/aruba-setup/iot-conf.png)

+ Enter the following information in the IoT transport popup:


![](../img/aruba-setup/iot-conf-2.png){width=80% height=80%}

![](../img/aruba-setup/iot-transport-conf3.png)

Click **OK** then **Save** to save you configuration.

+ Check that you transport stream is enabled:

![](../img/aruba-setup/iot-transport-conf4.png)

## Step 4: Verify that your Gateway is connected

Login to EnOcean IoT Connector and check gateway status using GET/gateways API</br>
![](../img/aruba-setup/aruba-check.png)
