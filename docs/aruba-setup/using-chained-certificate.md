# Using chained certificate

1. Get certificates files:

    In the browser navigate to  [https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/) and paste the URL of your IOTC EnOcean IoT Connector then click **Submit**

    <!--change pic below to contain details tab hovered -->
    ![](../img/aruba-setup/ssl-get-cert.png){width=50% height=50%}


2. Create certificate chain:

    ***Download the Server Certificate***

    ![](../img/aruba-setup/ssl-server-cert.png){width=50% height=50%}

    ***Download the Addtional Certificates***

    ![](../img/aruba-setup/ssl-additional-cert.png){width=50% height=50%}

    ***Create the Certificate Chain File***

    Combine the obtained server certificate and the additional certificates file into a single file using a text editor, first in chain file should be your Server’s certificate, second one should be the addtional certificates obtained in the previous step. The obtained certificate chain file should look like below:

    ![](../img/aruba-setup/ssl-chain.png){width=30% height=30%}

    <!--for self signed you can skip this step -->


3. Install the file in Aruba Instant.