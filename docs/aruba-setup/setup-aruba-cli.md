# Notes for Aruba APs configuration through CLI

## Required Hardware and Software

+ Aruba AP: Aruba AP with USB port. 

    Check the energy requirements of our Aruba AP to properly operate the USB port.</br>
    ![](../img/aruba-ap-power-req.png)

+ Aruba OS: version **8.10.0.0** or newer (most likely requires update to latest).

+ [EnOcean USB Stick](https://www.enocean.com/en/products/distributor/): USB 300, USB 300U, USB 500 or USB 500U

## Adding root certificates

By default the Aruba APs won't be able to connect to the IoT connector using a [self-signed certificate](./additional-notes.md#generating-self-signed-certificates). To fix this, it is possible to add an additional certificate by following these steps:

1. Log in into the AP's admin portal.
2. Go to the *Maintenance Section.*
3. Navigate to the *Certificates* sub-menu.
4. Click on *Upload New Certificate*.
5. Choose your root certificate, type in a name, select *Trusted CA* and click *Upload Certificate*.

## Configure Aruba AP to forward data to the IoTC

It is highly recommended to set-up the IoT Transport profile on Aruba AP through SSH. 

**Login into the AP using the same credentials from the web interface:**

```bash
$ ssh <yourUser>@<accesspointIP>
<youruser>@<accesspointIP>s password: <enter password>
```

Replace ` yourUser`, `accesspointIP` with your AP's credential's & IP-Address.

**After login:**

```bash
show tech-support and show tech-support supplemental are the two most useful outputs to collect for any kind of troubleshooting session.

aa:bb:cc:dd:ee:ff# configure terminal
We now support CLI commit model, please type "commit apply" for configuration to take effect.
aa:bb:cc:dd:ee:ff (config) # iot transportProfile myProfile
```

Replace `myProfile` with your desired profile name.

**Now configure the profile:**

=== "Aruba OS 8.8.0.0 and newer"

    ```bash
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # endpointType telemetry-websocket
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # endpointURL wss://myiotconnector:8080/aruba
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # payloadContent serial-data
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # authenticationURL https://myiotconnector:8080/auth/aruba
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # transportInterval 30
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # authentication-mode password
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # username <aruba_username set using IOT_GATEWAY_USERNAME>
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # password <aruba_password set using IOT_GATEWAY_PASSWORD>
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # endpointID 1111
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # end
    aa:bb:cc:dd:ee:ff# commit apply
    committing configuration...
    ```

=== "Aruba OS 8.10.0.0"

    ```bash
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # endpointType telemetry-websocket
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # endpointURL wss://myiotconnector:8080/aruba
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # payloadContent serial-data
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # authenticationURL https://myiotconnector:8080/auth/aruba
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # transportInterval 30
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # username <aruba_username set using IOT_GATEWAY_USERNAME>
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # password <aruba_password set using IOT_GATEWAY_PASSWORD>
    aa:bb:cc:dd:ee:ff (IoT Transport Profile "myProfile") # end
    aa:bb:cc:dd:ee:ff# commit apply
    committing configuration...
    ```


**Then activate the profile:**

```bash
aa:bb:cc:dd:ee:ff # configure terminal
We now support CLI commit model, please type "commit apply" for configuration to take effect.
aa:bb:cc:dd:ee:ff (config) # iot useTransportProfile myProfile
aa:bb:cc:dd:ee:ff (config) # end
aa:bb:cc:dd:ee:ff # commit apply
committing configuration...
configuration committed.
```

## Verify that your Gateway is connected

Login to EnOcean IoT Connector and check gateway status using GET/gateways API</br>
![](../img/aruba-setup/aruba-check.png)