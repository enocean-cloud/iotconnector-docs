# Configuration using Aruba Central

## Required Hardware and Software

+ Aruba AP: Aruba AP with USB port. 

    Check the energy requirements of our Aruba AP to properly operate the USB port. 

![](../img/aruba-ap-power-req.png)

+ Aruba OS: version **10.x**

+ [EnOcean USB Stick](https://www.enocean.com/en/products/distributor/): USB 300, USB 300U, USB 500 or USB 500U

## Step 1: Connect to Aruba Central

Log into the web-based management page for Aruba Central.

## Step 2: Install Collector

+  Under **Organization** click **Platform Integration** then **Data Collectors**.

![](../img/aruba-setup/aos10-collector-1.png)

+ Under the section **Configure Appliance** click **Download Virtual Appliance** (Small or Medium appliance recommended).
+ Follow the [collector installation guide](https://www.arubanetworks.com/techdocs/central/2.5.8/content/nms/apps/iot/iot-cfg.htm#Download) from Aruba.
+ Get registration token under **Registration Token**.
+ Click on **Create collector** and follow the steps required for the connection.

![](../img/aruba-setup/aos10-collector-2.png)

## Step 3: Configure Collector

+ Under **Applications** click on **IoT Operations** then **Connectors**.

![](../img/aruba-setup/aos10-collector-3.png)

+ Select your connector then click on **View** under **Installed Applications**

![](../img/aruba-setup/aos10-collector-4.png)

## Step 4: Install EnOcean App

+ To enable the USB dongle on the Aruba access points under AOS 10.x, you will need to install the EnOcean App.

![](../img/aruba-setup/aos10-enocean-app.png)

## Step 5: Install AOS8 App

+ AOS8 App is needed to enable data transfer to EnOcean Cloud.

![](../img/aruba-setup/aos10-aos8-app.png)

## Step 6: Configure AOS8 App

+ Configure the AOS8 App to send the data to EnOcean Cloud.

![](../img/aruba-setup/aos10-aos8-app-edit.png)

![](../img/aruba-setup/aos10-aos8-app-edit-2.png)

![](../img/aruba-setup/aos10-aos8-app-edit-3.png)

![](../img/aruba-setup/aos10-aos8-app-edit-4.png)

![](../img/aruba-setup/aos10-aos8-app-edit-5.png)

![](../img/aruba-setup/aos10-aos8-app-edit-6.png)

## Step 4: Verify that your Gateway is connected

Login to EnOcean IoT Connector and check gateway status using GET/gateways API</br>
![](../img/aruba-setup/aruba-check.png)