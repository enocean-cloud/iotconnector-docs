# Configuration using Aruba Central

## Required Hardware and Software

+ Aruba AP: Aruba AP with USB port. 

    Check the energy requirements of our Aruba AP to properly operate the USB port. 

![](../img/aruba-ap-power-req.png)

+ Aruba OS: version **8.10.0.13** or higher (most likely requires update to latest).

+ [EnOcean USB Stick](https://www.enocean.com/en/products/distributor/): USB 300, USB 300U, USB 500 or USB 500U

## Step 1: Connect to Aruba Central

Log into the web-based management page for Aruba Central.

## Step 2: Installing Trusted CA Certificates

!!! note
    When using Public Validated Certificates with Aruba Central, the complete certificate path (root + intermediate CAs) must be installed as **one file**. 

+  Upload your .pem certificate file From **Your group** -> **Organization** -> **Certificates**.



    ![](../img/aruba-setup/central-ca.png)

+  Select **CA certificate** as **certificate type** and **PEM** as **Certificate format** then choose your certificate .pem file.
    To create the certificate file chain for public validated certificates follow the guide : [Using chained certificate](./using-chained-certificate.md).

    ![](../img/aruba-setup/central-ca2.png)
    

+ Click **Add** to save your settings.

+ Verify the certificate is shown on the certificate list.

    ![](../img/aruba-setup/central-ca3.png)
    

## Step 3: IoT transport configuration

+ In your Aruba Central group select **Devices** -> **Config**:

    ![](../img/aruba-setup/central-iot.png)

+ select **IoT** then add a new IoT transport stream using the **+** icon:

    ![](../img/aruba-setup/central-iot2.png)

+ Enter the following information in the IoT transport tab:

    ![](../img/aruba-setup/central-iot3.png)

    ![](../img/aruba-setup/central-iot4.png)

    ![](../img/aruba-setup/central-iot5.png)

    ![](../img/aruba-setup/central-iot6.png)

    Click **Save settings** to save you configuration.

+ Check that you transport stream is enabled:
    ![](../img/aruba-setup/central-iot-verify.png)

## Step 4: Activating the certificate  

+ Under **Access Points** click **Security** then **Certificate Usage**.

    ![](../img/aruba-setup/central-iot8.png)

+ Tick your certificate then click **Save Settings**.

    ![](../img/aruba-setup/central-iot9.png)


## Step 5: Verify that your Gateway is connected

Login to EnOcean IoT Connector and check gateway status using GET/gateways API</br>
![](../img/aruba-setup/aruba-check.png)
