# Notes for Aruba APs Debugging & Troubleshooting


In case the Aruba AP (instant) is not connected to the IoTC i.e. the device is not listed in the gateway list or no EnOcean telegrams are visible on the egress of the IoTC. Try the following steps. Please consider that the commands syntax might change with new Aruba OS releases. The commands were tested with Aruba OS 8.8.x.

1. Show the IoT configuration. Get show and confirm the showed information correspond with the inputs provided before.

    ```powershell
    aa:bb:cc:dd:ee:ff # show iot transportProfile myProfile
    ```

2. Show & check connected USB devices. Example output is attached. For proper communication an `EnOcean USB` device must be connected to the AP.

    ```powershell
    aa:bb:cc:dd:ee:ff # show usb devices

    USB Device Info
    ---------------
    DeviceID  APMac Vendor ID  Product ID  Manufacturer  Product             Version  Serial    Class  Device   Driver    Uptime
    --------  ---------------  ----------  ------------  -------             -------  -------   -----  ------   ------    ------
    d3adas..  aa:.. 0403       6001        EnOcean GmbH  EnOcean USB 300 DC  2.00     FT55W4A9  tty    ttyUSB0  ftdi_sio  24m34s
    ```

3. Check the configured IoT Configuration status. `...` represents omitted information.

    ```powershell
    aa:bb:cc:dd:ee:ff # show ap debug ble-relay iot-profile
    
    ConfigID                                : xx
    
    ---------------------------Profile[myProfile]---------------------------
    
    authenticationURL                       : ...
    serverURL                               : ...
    ...
    --------------------------
    TransportContext                        : Connection Established
    Last Data Update                        : 2021-06-14 15:01:20
    Last Send Time                          : 2021-06-14 15:01:19
    TransType                               : Websocket
    ```

    If `TransportContext` displays an error message, please follow up on the meaning of the message. Please consider it can take few seconds to build the connection.

4. To check if EnOcean telegrams are being received and forwarded via the established connection please use the following command and watch if the `Websocket Write Stats` increases after a known EnOcean telegram transmission. Also check for changes in `Last Send Time`. `...` represents omitted information.

    ```powershell
    aa:bb:cc:dd:ee:ff # show ap debug ble-relay report

    ---------------------------Profile[myProfile]---------------------------

    WebSocket Connect Status                : Connection Established
    WebSocket Connection Established        : Yes
    Handshake Address                       : ...
    Refresh Token                           : Not Configured
    Access Token                            : ...
    Access Token Request by Client at       : 2021-06-14 14:18:32
    Access Token Expire at                  : 2021-06-14 15:18:32
    Location Id                             : ...
    Websocket Address                       : ...
    WebSocket Host                          : ...
    WebSocket Path                          : ...
    Vlan Interface                          : Not Configured
    Current WebSocket Started at            : 2021-06-14 14:18:42
    Web Proxy                               : NA
    Proxy Username&password                 : NA, NA
    Last Send Time                          : 2021-06-14 14:30:35
    Websocket Write Stats                   : 8278 (1454156B)
    Websocket Write WM                      : 0B (0)
    Websocket Read Stats                    : 0 (0B)
    ```

5. If there are any issues you can get additional log messages by running the following command.

    ```powershell
    aa:bb:cc:dd:ee:ff #  show ap debug ble-relay ws-log myProfile
    ```

    If you struggle with the connection of an Instant Aruba AP please contact the Aruba technical support.

    For debugging enterprise connected Aruba AP, via an [Aruba Controller](https://www.arubanetworks.com/en-gb/products/wireless/gateways-and-controllers/) please use these commands instead.

    ```powershell
    #Show profiles
    show iot transportProfile myProfile
    
    #Show USB devices
    show ap usb-device-mgmt all
    
    #Show status and report 
    show ble_relay iot-profile
    show ble_relay report <iot-profile-name>
    
    #Show Log
    show ble_relay ws-log <iot-profile-name>
    ```
