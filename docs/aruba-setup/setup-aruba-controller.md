# Configuration using Aruba Controller

## Required Hardware and Software

+ Aruba AP: Aruba AP with USB port. 

    Check the energy requirements of our Aruba AP to properly operate the USB port. </br>
    ![](../img/aruba-ap-power-req.png)

+ Aruba OS: version **8.10.0.13** or higher (most likely requires update to latest).

+ [EnOcean USB Stick](https://www.enocean.com/en/products/distributor/): USB 300, USB 300U, USB 500 or USB 500U

## Step 1: Connect to ArubaOS

Log into the web-based management page for ArubaOS.


## Step 2: Installing Trusted CA Certificates

!!! note
    When using Public Validated Certificates with Aruba Controller, the complete certificate path (root + intermediate CAs) must be installed as **separate files**.
     The server and additional certificates files could be downloaded from [https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/).

+ In your managed network tab select **Configuration** -> **System** -> **Certificates**
    Click the **+** to upload a new certificate.
    select Trusted CA as **certificate type** and **x509(.pem .cer or .crt)** as Certificate format.

    ![](../img/aruba-setup/controller-ca.png)

+ Click **Upload Certificate** to save your settings.

+ Verify that the certificate is shown on the certificates list.

    ![](../img/aruba-setup/controller-ca-verify.png){width=80% height=80%}


## Step 3: IoT transport configuration

+ In Aruba managed network select **Configuration** -> **IoT** -> **Transport streams** then add a new transport using the **+** icon.

    Enter the following information in the IoT transport tab:
    ![](../img/aruba-setup/controller-iot.png)
    ![](../img/aruba-setup/controller-iot2.png)
    ![](../img/aruba-setup/controller-iot3.png)
    ![](../img/aruba-setup/controller-iot4.png)

+ Check that you transport stream is enabled:
    ![](../img/aruba-setup/controller-iot-verify.png)

## Step 4: Verify that your Gateway is connected

Login to EnOcean IoT Connector and check gateway status using GET/gateways API</br>
![](../img/aruba-setup/aruba-check.png)
