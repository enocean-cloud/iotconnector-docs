# Micropelt iTRV MVA004

<div style="display: grid; grid-template-columns: 1fr 1fr; grid-template-rows: 1fr; grid-column-gap: 0px; grid-row-gap: 0px;">
<div>
<br>
Product Name: iTRV MVA004<br>
Product Description: Self-powered Radiator Valve<br>
Product Information page: <a href="https://www.micropelt.com/en/products/enocean/mva004">Link</a><br>
Product EEP: <a href="http://tools.enocean-alliance.org/EEPViewer/profiles/A5/20/01/A5-20-01.pdf">A5-20-01</a><br>
Product ID: 004900000005<br>
Product unlock code: FFFFFFFE<br>

</div>
<div>
<img src="../../img/bidirectional/TRV.png" width="400" style="display: block; margin-left: auto; margin-right: auto;" />
</div>
</div>



## Device Commissioning and Configuration

The device is commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). For the Micropelt iTRV MVA004 below specific information must be used in API.  

```json
{
  ...
  "deviceType": "bidirectional",
  "eep": "A5-20-01",
  "productId": "004900000005",
  "unlockCode": "FFFFFFFE"
}
```

<br>
Once the device is commissioned IoTC will automatically configure the device for bidirectional communication. This will be done when receiving telemetry data from the device, which is default sent every one hour. 
 

## Controlling Device

IoTC can control iTRV using valve position functionality of the device. To set new valve position use json below in `device/{device_id}/rpc/command` topic. The "valve" parameter can be configured between 0 and 100, as defined in EEP.
 
```json
{
    "request_id": 1,
    "eep": "A5-20-01",
    "payload": {
        "valve": 43,
        "temperature": 0,
        "runInitSequence":0,
        "runLiftSet":0,
        "valveOpen": 0,
        "valveClosed": 0,
        "summerMode": 0,
        "setPointSelection": 0,
        "setPointInverse": 1,
        "selectFunction": 0,
        "learnBit": 0
    }
}
```

