**Making a requests**

A request is made using the `device/{device_id}/rpc/command` topic with command schema as shown below. The command payload is using keys from the JSON representation of the EEP. More information about the payload is available in the description of the devices:

1. [Micropelt iTRV MVA004](micropelt.md)
2. [NodOn Multifunction Relay Switch](nodon-relay.md)
3. [NodOn Smart Plug](nodon-smart-plug.md)
4. [OPUS TRV](opus.md)

```json
{
  "request_id": 1,
  "eep": "A5-20-01",
  "payload": { 
    "summerMode": 0,
    "temperature": 0,
    "temperatureSetpoint": 0,
    ...
    }
}
```

| key | type and meaning |
| --- | --- |
| request_id | integer, unique identifier generated by application and used for tracking requests |
| eep | string, EnOcean Equipment Profile. This may be different from device's reporting EEP |
| payload | object, command payload using keys from JSON representation of EEP |

**Request results**

After making a request the application should examine the `device/{device_id}/rpc/result` topic from IoTC for result of the request.

```json
{
    "request_id": 1,
    "success": true,
    "message": "Command successfully queued"
}
```

|key|type and meaning|
|---|---|
|request_id|integer, unique identifier of the RPC request|
|success|boolean, status of request|
|message|string, explanation of request's current state|

<br>
If IoTC can properly transcode the request and put the command in queue, the application will receive below result:

```json
{
    "request_id": 1,
    "success": true,
    "message": "RPC Command successfully transcoded [buffer=55:00:0A:07:01:EB:A5:00:00:00:08:DE:AD:BE:EF:00:03:FF:FF:FF:FF:FF:00:58]"
}
```
<br>
Once data is sent from the queue to the device, the application will receive below result:

```json
{
    "request_id": 1,
    "success": true,
    "message": "RPC request sent to device"
}
```
<br>
Possible error messages are as below:

<br>
Device not added to IoTC
```json
{
  "request_id": 1,
  "success": false,
  "message": "Device does not exist"
}
```
<br>
Device exists in IoTC but not configured to be controlled
```json
{
  "request_id": 1,
  "success": false,
  "message": "Device is not configured for bidirectional communication"
}
```
<br>
Invalid json schema
```json
{
  "request_id": 1,
  "success": false,
  "message": "JSON decoding error. Given payload is not a valid JSON."
}
```
<br>
One of the required keys of EEP is missing
```json
{
  "request_id": 1,
  "success": false,
  "message": "Payload doesn't comply with given EEP(A5-20-01). [valve] is missing"
}
```
<br>
Device is not bidirectional
```json
{
  "request_id": 1,
  "success": false,
  "message": "Device does not support bidirectional communication"
}
```

**Canceling existing request**

A request may be canceled using the `device/{device_id}/rpc/cancel` topic with below schema.  

```json
{
  "request_id": 1,
}
```

|key|type and meaning|
|---|---|
|request_id|integer, unique identifier of the RPC request to be canceled|

<br>
After canceling a request, the application should examine the `device/{device_id}/rpc/result` topic from IoTC for result of the request.
```json
{
    "request_id: 1,    
    "success": true,
    "message": "RPC command cancelled"
}
```