# Overview
Bidirectional communication was added with IoTC v1.6 to support sending commands from IoTC to EnOcean devices. This allows controlling TRV's and switching ON/OFF light relay switches. Communication from IoTC to EnOcean devices is managed through MQTT using RPC topics (Remote Procedure Call). To support sending data to energy harvesting devices, IoTC also supports queuing commands until the device is ready. The application can manage data in the queue and will get status notifications from IoTC through MQTT RPC topics.

## Commissioning of Bidirectional Devices
Bidirectional devices are commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). Once a device is commissioned it must be configured for bidirectional communication. As the configuration procedure is device specific, it is required to follow the configuration procedure described for the specific device. The following list of devices are currently supported: 

1. [Micropelt iTRV MVA004](micropelt.md)
2. [NodOn EnOcean Multifunction Relay Switch](nodon-relay.md)
3. [NodOn EnOcean Smart Plug](nodon-smart-plug.md)
4. [OPUS TRV](opus.md)

## Controlling bidirectional devices
Communication from IoTC to EnOcean devices is managed through MQTT using RPC topics (Remote Procedure Call). The communication and RPC topic flow is shown in below diagrams. To send a command the application will initiate a request using the `device/{device_id}/rpc/command` topic. IoTC puts the command in the queue and returns a status notification using the `device/{device_id}/rpc/result` topic. The queue will examine if the device is an energy harvesting device or if the command can be sent immediatly to the device. In case the device is an energy harvesting device, IoTC will keep the command in queue until receiving any data from the device like regular telemetry data. Data from the device is used to indicate the device is awake and can receive data. Once data is sent to the device IoTC will notify the application using the `device/{device_id}/rpc/result` topic. 

<br>

<img src="../../img/bidirectional/communication_flow_energy_harvesting.png" width="950" style="display: block; margin-left: auto; margin-right: auto;" />
<br>
<div style="text-align: center;">
Communication and RPC topic flow for sending data to an energy harvesting device
</div>

<br>

<img src="../../img/bidirectional/communication_flow_main_power.png" width="950" style="display: block; margin-left: auto; margin-right: auto;" />
<br>
<div style="text-align: center;">
Communication and RPC topic flow for sending data to an always on device
</div>



<br>

The application may cancel data in the queue by using the `device/{device_id}/rpc/cancel` topic as shown in below diagram. IoTC will cancel the request and notify the application using the `device/{device_id}/rpc/result` topic.

<br>

<img src="../../img/bidirectional/communication_flow_cancel_cmd.png" width="950" style="display: block; margin-left: auto; margin-right: auto;" />
<br>
<div style="text-align: center;">
RPC topic flow for canceling command in queue
</div>
