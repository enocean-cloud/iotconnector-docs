# Eltako FSVA-230V-10A

<div style="display: grid; grid-template-columns: 1fr 1fr; grid-template-rows: 1fr; grid-column-gap: 0px; grid-row-gap: 0px;">
<div>
<br><br><br>
Product Name: FSVA-230V-10A<br>
Product Description: Wireless actuator switch<br> 
Product Information page: <a href="https://www.eltako.com/en/product/professional-standard-en/three-phase-energy-meters-and-one-phase-energy-meters/fsva-230v-10a/">Link</a><br>
Product EEP: <a href="http://tools.enocean-alliance.org/EEPViewer/profiles/A5/12/01/A5-12-01.pdf">A5-12-01</a><br>
Product ID: 000D30100003<br>

</div>
<div>
<img src="../../img/bidirectional/Eltako.png" width="250" style="display: block; margin-left: auto; margin-right: auto;" />
</div>
</div>

## Device Commissioning and Configuration
The device is commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). For the Eltako FSVA-230V-10A below specific information must be used in API. 

```json
{
  ...
  "deviceType": "bidirectional",
  "eep": "A5-12-01",
  "productId": "000D30100003",
}
```
<br>
Once the device is commissioned, IoTC will need to configure the device for bidirectional communication. To enable device configuration, follow below setps:

- Make sure device is factory reset
- Plug in the device while pressing both buttons on the device to enable reporting ON/OFF
- Within 5 second of plugging in the device enable learn-in mode by press left button for 3 seconds until device starts blinking red led then press right button two times quickly
- IoTC will try to configure the device after 5 seconds and expect device to be in learn-in mode 
- After configuring the device, IoTC will toggle the device two times to make sure it's configured for bidirectional communication. You should notice the device switching ON/OFF 
- Device is configured

## Controlling Device

To turn ON the device use json below in `device/{device_id}/rpc/command` topic.

```json
{
    "request_id": 1,
    "eep": "f6-02-01",
    "payload": {
        "R1": 0,
        "EB": 1,
        "R2": 0,
        "SA": 0
    }
}
```
<br>
To turn OFF the device use json below in `device/{device_id}/rpc/command` topic.
```json
{
    "request_id": 1,
    "eep": "f6-02-01",
    "payload": {
        "R1": 1,
        "EB": 1,
        "R2": 0,
        "SA": 0
    }
}
```
