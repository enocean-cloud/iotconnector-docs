# OPUS Micropelt TRV

<div style="display: grid; grid-template-columns: 1fr 1fr; grid-template-rows: 1fr; grid-column-gap: 0px; grid-row-gap: 0px;">
<div>
<br>
Product Name: OPUS Micropelt TRV<br>
Product Description: Self-powered Radiator Valve<br>
Product Information page: <a href="https://www.micropelt.com/en/products/enocean/mva004">Link</a><br>
Product EEP: <a href="http://tools.enocean-alliance.org/EEPViewer/profiles/A5/20/01/A5-20-06.pdf">A5-20-06</a><br>
Product ID: 00401000003F<br>
Product unlock code: 53C65E34<br>

</div>
<div>
<img src="../../img/bidirectional/opus_trv.jpg" width="400" style="display: block; margin-left: auto; margin-right: auto;" />
</div>
</div>


## Device Commissioning and Configuration

The device is commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). For the OPUS Micropelt TRV below specific information must be used in API.  

```json
{
  ...
  "deviceType": "bidirectional",
  "eep": "A5-20-06",
  "productId": "00401000003F",
  "unlockCode": "53C65E34"
}
```

<br>
Once the device is commissioned IoTC will automatically configure the device for bidirectional communication. This will be done when receiving telemetry data from the device, which is default sent every one hour. 
 

## Controlling the Device
Opus TRVs manage valve position automatically when used with "Temperature Set Point" mode.

To control a TRV, a RPC command should be sent to respectetive MQTT topics. 
To `device/{eurid}/rpc/command` topic json below should be sent when a temperature change request is made.


```json
{
  "request_id": 1,
  "eep": "a5-20-06",
  "payload": {
    "temperatureSetpoint": 35,
    "temperature": 20,
    "referenceRun": 0,
    "rfConnectionInterval": 0,
    "summerBit": 0,
    "setPointSelection": 1,
    "temperatureSelection": 1,
    "standBy": 0,
    "learnBit": 1
  }
}
```

Important keys are `temperatureSetpoint` and `temperature`. 

**temperatureSetpoint** is the desired room temperature.

**temperature** is the current room temperature that is measured by a RCU.

MQTT Topics and expected information can be found [here](mqtt-interface.md) in detail.

