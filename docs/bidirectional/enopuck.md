# Deuta EnoPuck CO2MIC / CE

<div style="display: grid; grid-template-columns: 1fr 1fr; grid-template-rows: 1fr; grid-column-gap: 0px; grid-row-gap: 0px;">
<div>
<br><br><br>
Product Name: Deuta EnoPuck CO2MIC / CE<br>
Product Description: Wireless actuator switch<br> 
Product Information page: <a href="https://deuta-controls.net/gerate-peripherie/enopuck-co2mic/?lang=en">Link</a><br>
Product EEP: Product has multiple EEPs. Please refer to product page above. <br>
Product ID: 004600010005<br>

</div>
<div>
<img src="../../img/bidirectional/enopuck.jpg" width="250" style="display: block; margin-left: auto; margin-right: auto;" />
</div>
</div>

## Device Commissioning and Configuration
The device is commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). For the  Deuta EnoPuck CO2MIC / CE below specific information must be used in API. 

```json
{
  "eurid": "FFAC68E4" // First sensor of device will be used as eurid. BASEID
  "deviceType": "bidirectional",
  "eep": "A5-09-04",
  "productId": "004760202868",
  "customTag": "042258d1" // Actual EnOcean ID will be given as customTag
}
```
<br>
Once the device is commissioned, it will be ready for bidirectional control of the LEDs. No pairing procedure is needed.

## Controlling Device

To control the device use json below in `device/{device_id}/rpc/command` topic. You can create colors by changing the RGB color mix. Command below will light the LED white with full brightness.

```json
{
    "request_id": 1,
    "eep": "ff-ff-ff",
    "payload": {
        "R": 100, // Red channel percentage 0-100
        "G": 100, // Green channel percentage 0-100
        "B": 100, // Blue channel percentage 0-100
        "A": 100 // Brightness channel percentage 0-100
    }
}
```
<br>
To turn OFF the LED use json below in `device/{device_id}/rpc/command` topic.
```json
{
    "request_id": 1,
    "eep": "ff-ff-ff",
    "payload": {
        "R": 0, // Red channel percentage 0-100
        "G": 0, // Green channel percentage 0-100
        "B": 0, // Blue channel percentage 0-100
        "A": 0 // Brightness channel percentage 0-100
    }
}
```
<br>
As an example to light the LEDs red use json below in `device/{device_id}/rpc/command` topic.
```json
{
    "request_id": 1,
    "eep": "ff-ff-ff",
    "payload": {
        "R": 100,
        "G": 0,
        "B": 0,
        "A": 100
    }
}
```