# Controlling bidirectional devices

Communication from IoTC to EnOcean devices is managed through MQTT using RPC topics (Remote Procedure Call). The communication and RPC topic flow is shown in below diagrams. To send a command the application will initiate a request using the `device/{device_id}/rpc/command` topic. IoTC puts the command in the queue and returns a status notification using the `device/{device_id}/rpc/result` topic. The queue will examine if the device is an energy harvesting device or if the command can be sent immediatly to the device. In case the device is an energy harvesting device, IoTC will keep the command in queue until receiving any data from the device like regular telemetry data. Data from the device is used to indicate the device is awake and can receive data. Once data is sent to the device IoTC will notify the application using the `device/{device_id}/rpc/result` topic. 

<br>

![](../img/bidirectional/communication_flow_energy_harvesting.png)
_Communication and RPC topic flow for sending data to an energy harvesting device_

<br>

![](../img/bidirectional/communication_flow_main_power.png)
_Communication and RPC topic flow for sending data to an always on device_

<br>

The application may cancel data in the queue by using the `device/{device_id}/rpc/cancel` topic as shown in below diagram. IoTC will cancel the request and notify the application using the `device/{device_id}/rpc/result` topic.

<br>

![](../img/bidirectional/communication_flow_cancel_cmd.png)
_RPC topic flow for canceling command in queue_
