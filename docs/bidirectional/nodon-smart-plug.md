# NodOn Smart Plug

<div style="display: grid; grid-template-columns: 1fr 1fr; grid-template-rows: 1fr; grid-column-gap: 0px; grid-row-gap: 0px;">
<div>
<br><br><br>
Product Name: NodOn Smart Plug<br>
Product Description: Wireless actuator switch<br> 
Product Information page: <a href="https://nodon.fr/en/nodon/enocean-metering-smart-plug/">Link</a><br>
Product EEP: <a href="http://tools.enocean-alliance.org/EEPViewer/profiles/D2/01/0B/D2-01-0B.pdf">D2-01-0B</a><br>
Product ID: 004600010005<br>

</div>
<div>
<img src="../../img/bidirectional/nodon-smartplug.jpg" width="250" style="display: block; margin-left: auto; margin-right: auto;" />
</div>
</div>

## Device Commissioning and Configuration
The device is commissioned using the `POST /devices` API as described [here](../guides/how-to-onboard-sensors.md). For the  NodOn Smart Plug below specific information must be used in API. 

```json
{
  ...
  "deviceType": "bidirectional",
  "eep": "D2-01-0B",
  "productId": "004600010005",
}
```
<br>
Once the device is commissioned, IoTC will need to configure the device for bidirectional communication. To enable device configuration, follow below setps:

- Make sure device is factory reset
- Press the button for 2 seconds. Device will go in pairing mode, led will flash red.
- IoTC will configure the device automatically
- Device is configured

## Controlling Device

To turn ON the device use json below in `device/{device_id}/rpc/command` topic.

```json
{
    "request_id": 1,
    "eep": "f6-02-01",
    "payload": {
        "R1": 0,
        "EB": 1,
        "R2": 0,
        "SA": 0
    }
}
```
<br>
To turn OFF the device use json below in `device/{device_id}/rpc/command` topic.
```json
{
    "request_id": 1,
    "eep": "f6-02-01",
    "payload": {
        "R1": 0,
        "EB": 0,
        "R2": 0,
        "SA": 0
    }
}
```
