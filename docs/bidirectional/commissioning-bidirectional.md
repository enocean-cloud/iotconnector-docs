Bidirectional devices are commissioned using the `POST /devices` API as described [here](https://iotconnector-docs.readthedocs.io/en/release-1.6.0/guides/how-to-onboard-sensors/). Once a device is commissioned it must be configured for bidirectional communication. As the configuration procedure is device specific, it is required to follow the configuration procedure described for the specific device. The following list of devices are currently supported: 

1. [Micropelt iTRV MVA004](micropelt.md)
2. [Eltako FSVA-230V-10A](eltako.md)