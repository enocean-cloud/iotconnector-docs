A device is commissioned with IoTC using the `POST /device` API with the json schema below. Some parameters are optional depending on if the device is secure included or supports bidirectional communication.  


```json
{
  "friendlyid": "Room Panel 02",
  "eep": "A5-04-05",
  "deviceType": "sensor",
  "eurid": "A1B2C3D4",
  "destinationEurid": "FFFFFFFF",
  "location": "Level 2 / Room 221",
  "slf": "f3",
  "AESKey": "3A0C1B30B0A822A17A28FD01D77ABDAE"
  "productId": "004900000005",
  "unlockCode": "FFFFFFFE"
}
```

| Key | Required or Optional | Type | Description |
| --- | --- |--- |--- |
| friendlyid | always required | string | Device name used in IoTC |
| eep | always required | string | EnOcean Equipment Profile |
| deviceType | always required | string | Device type defines communication between device and IoTC |
| eurid | always required | string | EnOcean device specific ID |
| destinationEurid | always required | string | Used to address communication from device to IoTC. Must always be set to FFFFFFFF |
| location | always required | string | Device location used in IoTC |
| slf | required for secure devices | string | Security level format as defined by the EnOcean Alliance. Currently only f3 is supported |
| AESKey | required for secure devices | string | Device AES key used for secure encryption |
| product_id | required for bidirectional communication | string | Device product ID, used by IoTC to identify how to configure device for bidirectional communication  |
| unlock_code | required for bidirectional communication | string | May be required to configure device for bidirectional communication |


<br>
Supported values in `deviceType`  

| Key | Description |
| --- |--- |
| sensor | Devices only sending data to IoTC |
| bidirectional | Devices supporting bidirectional communication |
| switch | Wall switch devices based on EnOcean PTM module |