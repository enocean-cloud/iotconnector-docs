# Prerequisites and used third party libraries

## Technical Requirements

The different containers of the IoTC require the [Docker](https://docs.docker.com/get-started/overview/) environment to run. Specific requirements (i.e. RAM, CPU) depend on the number of connected end points to the IoTC at runtime and their communication frequency. Typical installations (e.g. 100 connected AP, 500 EnOcean end points).

The IoTC was load tested in laboratory conditions with 200 Gateways / APs with trasnfering in total 2000 EnOcean Messages within 10 seconds. No issues or message lose was detected.

For Azure Cloud deployments we recommend to use the `docker-compose.yml` file listed in  `azure_deployment` directory.

## Used 3rd party components and libraries, OSS Components

**Components:**

- Redis Community(https://redis.io/)
- Python 3.8 (https://www.python.org/)
- Docker Community (https://docs.docker.com/get-docker/)
- NGINX Community (https://www.nginx.com/)
- Mosquitto (https://mosquitto.org/)


 **Python Libraries:**

- Async Redis (aioredis,https://github.com/aio-libs/aioredis-py, MIT License)
- HIREDIS (hiredis,https://github.com/redis/hiredis,BSD License)
- Licensing (licensing,https://github.com/Cryptolens/cryptolens-python,MIT License)
- Protobuf (protobuf,https://developers.google.com/protocol-buffers/,https://github.com/protocolbuffers/protobuf/blob/master/LICENSE)
- Pydantic (pydantic,https://github.com/samuelcolvin/pydantic/,MIT License)
- Redis (redis,https://github.com/andymccurdy/redis-py,MIT License)
- Tornado (tornado,https://github.com/tornadoweb/tornado,Apache License 2.0)
- Flask (flask,https://flask.palletsprojects.com/en/1.1.x/,BSD=https://flask.palletsprojects.com/en/0.12.x/license/)
- Conexion (conexion,https://github.com/zalando/connexion,https://github.com/zalando/connexion/blob/master/LICENSE.txt)
- Azure (azure,https://github.com/Azure/azure-sdk-for-python,MIT)
- Bitstring (bitstring,https://github.com/scott-griffiths/bitstring,MIT)
- crc8 (crc8,https://github.com/niccokunzmann/crc8,MIT)
- paho-mqtt (paho-mqtt,http://www.eclipse.org/paho/,BSD=https://projects.eclipse.org/projects/iot.paho)
- pycryptodome (pycryptodome, https://github.com/Legrandin/pycryptodome,https://github.com/Legrandin/pycryptodome/blob/master/LICENSE.rst)
- Celery (celery,https://github.com/celery/celery,https://github.com/celery/celery/blob/master/LICENSE)
