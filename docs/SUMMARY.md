* Overview
    * [Overview](index.md)
    * [Prerequisites](prerequisites.md)
* Deployment
    * [Deploy with Docker Compose](deploy-the-iotc.md)
    * [Generating self-signed certificates](additional-notes.md)
    * [Configuration](deployment-notes.md)
    * [Add sensors](guides/how-to-onboard-sensors.md)
* Setup Aruba AP
    * [Aruba Instant](aruba-setup/setup-aruba-instant.md)
    * [Aruba Controller](aruba-setup/setup-aruba-controller.md)
    * [Aruba Central (AOS 8)](aruba-setup/setup-aruba-central.md)
    * [Aruba Central (AOS 10)](aruba-setup/setup-aruba-central-aos10.md)
    * [Aruba CLI](aruba-setup/setup-aruba-cli.md)
    * [Debugging & Troubleshooting](aruba-setup/debug-aruba.md)
    * [Using chained certificate](aruba-setup/using-chained-certificate.md)
* API Reference
    * [Swagger API](api_reference/api-documentation.md)
    * [MQTT API](api_reference/mqtt-API.md)
    * [API Usage](api_reference/API-usage.md)
* Integration
    * [MQTTs](integration/MQTTS.md)
    * [ElasticSearch](integration/elasticsearch.md)
    * [Kibana](integration/kibana.md)
    * [Thingsboard](integration/thingsboard.md)
    * [Azure IoT Central](integration/iot_central.md)
    * [Azure IoT Hub](integration/iot_hub.md)
* Bidirectional communication
    * [Overview](bidirectional/overview.md)
    * [MQTT Interface](bidirectional/mqtt-interface.md)
    * Devices
        * [Micropelt TRV004](bidirectional/micropelt.md)
        * [NodOn Multifunction Relay Switch](bidirectional/nodon-relay.md)
        * [NodOn Smart Plug](bidirectional/nodon-smart-plug.md)
        * [OPUS TRV](bidirectional/opus.md)
* Troubleshooting
    * [System health information](guides/how-to-get-system-health.md)
    * [Debugging](guides/debugging.md)
    * Best Practices
        * [Azure cost estimation](runtime-azure.md)
        * [Performance tests](runtime-performance.md)
* Update Notes
    * [Updating EIoTC to Latest Version](update-notes/tolatest.md)
    * [Updating from v1.3 to 1.4](update-notes/13to14.md)
    * [Updating from v1.2 to 1.3](update-notes/12to13.md)
    * ChangeLog
        * [EnOcean IoT Connector - 1.9.0 Released](release-notes/10023-release.md)
        * [EnOcean IoT Connector - 1.8.0 Released](release-notes/10022-release.md)
        * [EnOcean IoT Connector - 1.7.0 Released](release-notes/10021-release.md)
        * [EnOcean IoT Connector - 1.6.0 Released](release-notes/10020-release.md)
        * [EnOcean IoT Connector - 1.5.0 Released](release-notes/10018-release.md)
        * [EnOcean IoT Connector - 1.4.0 Released](release-notes/10013-release.md)
        * [EnOcean IoT Connector - 1.3.1 Released](release-notes/10017-release.md)
        * [EnOcean IoT Connector - 1.3.0 Released](release-notes/10012-release.md)
        * [EnOcean IoT Connector - 1.2.0 Released](release-notes/10011-release.md)
        * [EnOcean IoT Connector - 1.1.0 Released](release-notes/10003-release.md)
        * [EnOcean IoT Connector - hotfix-engine-1.0.1 Released](release-notes/10010-release.md)
        * [EnOcean IoT Connector - 1.0.0 Released](release-notes/10009-release.md)
        * [Beta Releases](release-notes/beta-releases.md)
* About
    * [Downloads & Revision](downloads.md)
    * [Support](support.md)
    
