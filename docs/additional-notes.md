# Generating self-signed certificates

!!! warning
    Self-signed certificates are inherently insecure (since they lack a chain of trust). Please contact your IT Admin if you are unsure/unaware of the consequences of generating & using self-signed certificates. These instructions should be used for development environments only.

**For Windows users**: Use the `openssl` Docker image or `WSL` to generate a CA, CSR and finally a certificate.

**For Linux users**: Since most Linux distributions already include `openssl` there is no need to use docker for this step. Simply run the command directly by removing the initial call to docker:`docker run -it --rm -v ${PWD}:/export frapsoft/`. 

!!! note
    Execute the commands below from **iotconnector-docs/deploy/nginx** directory to simplify the process.

## Generate private key for CA authority:

=== "Windows"

    ```bash
    docker run -it --rm -v ${PWD}:/export frapsoft/openssl genrsa -des3 -out /export/myCA.key 2048
    ```

=== "Linux"

    ```bash
    openssl genrsa -des3 -out myCA.key 2048
    ```




## Generate root certificate

Complete the fields with the information corresponding to your organization.

!!! warning
    When prompted for Common Name enter the FQDN of the deployment or the machine IP address for local test deployments.

=== "Windows"

    ```bash
    docker run -it --rm -v ${PWD}:/export frapsoft/openssl req -x509 -new -nodes -key /export/myCA.key -sha256 -days 3650 -out /export/myCA.pem
    ```

=== "Linux"

    ```bash
    openssl req -x509 -new -nodes -key myCA.key -sha256 -days 3650 -out myCA.pem
    ```

For common name enter the hostname of the deployment or `localhost` for local test deployments.

## Generate a key for the certificate going into the connector

=== "Windows"

    ```bash
    docker run -it --rm -v ${PWD}:/export frapsoft/openssl genrsa -out /export/dev.localhost.key 2048
    ```

=== "Linux"

    ```bash
    openssl genrsa -out dev.localhost.key 2048
    ```


## Generate a CSR for the connector

=== "Windows"

    ```bash
    docker run -it --rm -v ${PWD}:/export frapsoft/openssl req -new -key /export/dev.localhost.key -out /export/dev.localhost.csr
    ```

=== "Linux"

    ```bash
    openssl req -new -key dev.localhost.key -out dev.localhost.csr
    ```


For common name enter the FQDN of the deployment or `localhost` for local test deployments.

## Create the .ext file

Create a new `localhost.ext` file with the following contents:

```bash
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
subjectAltName = @alt_names
subjectKeyIdentifier = hash

[alt_names]
DNS.1 = localhost
IP.1 = 192.168.1.2
```
!!! warning
    Edit the `localhost.ext` file to match your domain. Make sure the `DNS.1` matches the FQDN of your deployment. If you're accessing the deployment via IP address make sure the `IP.1` matches the IP address.

## Generate a certificate from CSR for the connector

=== "Windows"

    ```bash
    docker run -it -v ${PWD}:/export frapsoft/openssl x509 -req -in /export/dev.localhost.csr -CA /export/myCA.pem -CAkey /export/myCA.key -CAcreateserial -out /export/dev.localhost.crt  -days 825 -sha256 -extfile /export/localhost.ext
    ```

=== "Linux"

    ```bash
    openssl x509 -req -in dev.localhost.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial -out dev.localhost.crt  -days 825 -sha256 -extfile localhost.ext
    ```

Keep the generated key files safe and without access of 3rd parties.