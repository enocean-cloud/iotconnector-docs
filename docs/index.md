# EnOcean IoT Connector

The [EnOcean IoT Connector](https://iot.enocean.com/) (IoTC) allows for the easy processing of the super-optimized [EnOcean](https://www.enocean.com) radio telegrams. The IoTC is distributed as a group of [Docker](https://docs.docker.com/get-started/overview/) containers. All containers are hosted in the [Docker Hub](https://hub.docker.com/u/enocean).

![](./img/enocean-iot-connector-introduction-new2.svg)

The IoTC is composed of the following containers:

1. [enocean/iotconnector_ingress](https://hub.docker.com/repository/docker/enocean/iotconnector_ingress)
2. [enocean/iotconnector_engine](https://hub.docker.com/repository/docker/enocean/iotconnector_engine)
3. [enocean/iotconnector_api](https://hub.docker.com/repository/docker/enocean/iotconnector_api)
4. [enocean/iotconnector_integration](https://hub.docker.com/repository/docker/enocean/iotconnector_integration)
5. [Redis](https://hub.docker.com/_/redis)
6. [NGINX](https://hub.docker.com/_/nginx)

Deploying the IoTC is simple using `docker compose`. For convenience, `docker-compose.yml` files are provided to easily deploy locally (i.e. with [Docker](https://docs.docker.com/get-docker/)) or to [Azure Containers Instances](https://azure.microsoft.com/services/container-instances/) (Microsoft Azure cloud [account](https://azure.microsoft.com/free/) and subscription required).

The IoTC can either be deployed in:

- a public cloud (eg. Azure, AWS)
- private cloud
- on-premise

#### Documentation Version/Tag/SHA

`{{  git.tag }}` / ` {{git.date}}` / `{{ git.short_commit}}`

## Features

### Ingress

The ingress controls all incoming traffic from ingress gateways.

- Ingress has a secure web socket support for communication with APs.
- Ingress removes duplicates of data arriving from one sensor via several APs.
- Ingress processes the [ESP3 Protocol](https://www.enocean.com/wp-content/uploads/Knowledge-Base/EnOceanSerialProtocol3.pdf). Only Packet Type 01 & 10 are supported currently.
- The IoTC currently supports [Aruba Access Points](https://www.enocean.com/en/applications/iot-solutions/) as ingress gateways. This list is expanding.


### Engine

The IoTC engine completely supports the [EnOcean radio protocol](https://www.enocean.com/en/support/knowledge-base/) standards as defined by the [EnOcean Alliance](https://www.enocean-alliance.org/specifications/). Additionally, engine evaluates sensor health information, as well as the overall health of EIoTC solution:

- addressing encapsulation
- chaining
- decryption & validation of secure messages
- EEP processing
- information included in [signal telegram](https://www.enocean-alliance.org/st/)
- telegram statistics
- health check status

See the [Output format description](#output-format) for more details on what the engine can provide.

The following [EEPs](https://www.enocean-alliance.org/wp-content/uploads/2020/07/EnOcean-Equipment-Profiles-3-1.pdf) are supported:

{%
   include-markdown "./eep.md"
%}

A complete description and a list of all existing EEPs can be found here: [EEP Viewer](http://tools.enocean-alliance.org/EEPViewer/). If you are missing an EEP for your application please drop us an email on support-at-enocean-dot-com.

### API

The full API Specification is available [here](./api_reference/api-documentation.md) or via the web Interface, once the IoTC has been deployed.

The most important features are:

- onboard / update / remove enocean devices
- get most recent data and signal telegrams of a device
- telegram statistic (e.g. count, last seen) for a device and per gateway
- list of connected ingress gateways
- persistent storage of onboarded device - if you specified a volume storage at [deployment](./deploy-the-iotc.md#1-step-by-step-deployment)
- EIoTC health check status

The API container exposes a Web UI for your convenience to see the full documentation and to have a simple client interaction.

### Integration

Integration serves as the interface between EIoTC and external systems such as various cloud services that let you build your IoT solution. Currently, we support following integration methods:
- MQTT and MQTTS (IoTC acts as a client to an external MQTT broker)
- [Azure IoT Hub](https://azure.microsoft.com/en-us/services/iot-hub/)
- [Azure IoT Central](https://azure.microsoft.com/en-us/services/iot-central/) 

The output data format is JSON, in accordance to the  `key-value` pairs defined by the [EnOcean Alliance IP Specification](http://tools.enocean-alliance.org/EEPViewer/).

### NGINX

NGINX is used as a [reverse proxy](https://www.nginx.com/) to secure IoTC. It requires valid security certificates for operation. 

A `Dockerfile` / `azure.dockerfile` and corresponding dependencies (`start.sh` and `nginx.conf`) are provided at `/deploy/nginx/` in case it needs to be rebuilt or customized.

### Redis

Redis is used as a [message broker & cache](https://redis.io/) for communication between different containers.


## License Agreement and Data Privacy

Please see the License agreement [here](./LA-IoTC.pdf).

Please see the Data privacy agreement [here](./DPA-IoTC.pdf).

## Disclaimer

The information provided in this document describes typical features of the EnOcean software products and should not
be misunderstood as specified operating characteristics. No liability is assumed for errors and / or omissions. We
reserve the right to make changes without prior notice.
