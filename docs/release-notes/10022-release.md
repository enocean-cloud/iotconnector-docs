# EnOcean IoT Connector - 1.8.0

## General

### Features


- Added support for bidirectional communication for following devices:

    - Micropelt OPUS TRV: More information about bidirectional communication is available [here](../bidirectional/overview.md).

- Added support for the following EEPs:

    - D2-01-0F
    - D2-01-0B
    - D1-07-11
    - D1-09-01