# EnOcean IoT Connector - 1.4.0 Released

## General

### Features

- Updated API to v2
- Moved interservice messaging to RabbitMQ
- Reworked logging to have clear and standardized messages in all containers

## API Container

### Features

- Added ability to download logs from API

## Proxy Container

### Features

- Added health check for proxy container

