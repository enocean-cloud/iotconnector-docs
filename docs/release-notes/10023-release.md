# EnOcean IoT Connector - 1.9.0

## General

### Features

- Added support for new devices:
    - Pressac Air Quality Sensor [link](https://www.pressac.com/airquality/)
    - Jumitech CO2 Sensor

- Added support for the following EEPs:
    - D2-14-5C
    - D2-14-58
    - D2-14-59