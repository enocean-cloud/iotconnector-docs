# EnOcean IoT Connector - 1.5.0 Released

## General

### Features

- Added support for Deuta EnoSense CO2 Sensor with EEP D1-09-10

## Azure

### Bugs

- Fixed Azure deployment issue that was caused by resource reservation rules of Azure ACI
- Fixed issue preventing Azure IoT Hub and MQTT to co-exist. The issue was introduced in IoTC v1.4, when switching to use RabbitMQ
