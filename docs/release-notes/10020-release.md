# EnOcean IoT Connector - 1.6.0 Released

## General

### Features

- Added support for bidirectional communication, allowing the application to send commands to EnOcean devices. Below list of devices are currently supported: 
    - Eltako FSVA-230V
    - Micropelt TRV
    More information about bidirectional communication is available [here](../bidirectional/overview.md).
- A license grace period is introduced to the license check to avoid disruptions in case IoTC is unable to connect with the license server for a short period of time. 
