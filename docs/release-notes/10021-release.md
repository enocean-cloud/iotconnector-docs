# EnOcean IoT Connector - 1.7.0 Released

## General

### Features

- Added support for bidirectional communication for following devices: 
    - NodOn Multifunction Relay Switch
    - NodOn Smart Plug
    
    More information about bidirectional communication is available [here](../bidirectional/overview.md).
