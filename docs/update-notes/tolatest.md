# Updating EIoTC 

**IMPORTANT** for updating from IoTC v1.3 to IoTC v1.4 see [update notes](13to14.md)

**IMPORTANT** for updating from IoTC v1.2 to IoTC v1.3 see [update notes](12to13.md)

<br>
The IoTC application follows a quarterly release cycle for introduction of new features. Updating IoTC to latest release is simple using docker. The following describes the steps for both local and Azure deployments. 

1. Make a GET request to /api/v2/system/backup endpoint to generate a system backup

2. Update IoTC using docker

    **Local Deployment**
    ```shell
    #Stop deployment
    docker compose down
    
    #Pull latest versions of docker images
    docker compose pull
     
    #Start deployment
    docker compose up
    ```
    **Azure Deployment**
    ```shell
    #Stop deployment
    docker compose down
    
    #Start deployment
    docker compose up
    ```